
#' Simple plot of sample autocorrelations for a given kernel
#' 
#' Uses \code{sample_acfs()} to estimate autocorrelation as a function of lag.
#' This is done for a number of different sample parameters and data. The resulting 
#' estimates are then plotted.
#' @inheritParams sample_acfs
#' @param alpha Passes to \code{\link[ggplot2]{geom_line}} and \code{\link[ggplot2]{geom_point}}.
#' @return A \code{ggplot} object
#' @export 
plot_sample_acfs <- function(object, paths = 50, alpha = 0.3, steps = 500, thinning = 1, ...) {
  df <- sample_acfs(object, paths=paths, steps=steps, thinning=thinning, ...)
  p <- ggplot2::ggplot(df, ggplot2::aes(x = .data$lag, y = .data$corr, group = .data$lag))
  p <- p + ggplot2::geom_line(ggplot2::aes(group = .data$id), alpha = alpha) + ggpubr::theme_pubr() + ggplot2::ylab("Empirical Correlation") + xlab("Lag")
  if (max(df$stat) > 1) {
  p <- p + ggplot2::facet_wrap(~stat) + ggpubr::theme_pubr() +
    ggplot2::theme(
      strip.background = ggplot2::element_blank(), 
      strip.text = ggplot2::element_text(face = "bold")
    )
  }
  return(p)
}

#' Estimate autocorrelation for one chain applied to one sample of parameters 
#' and data
#'
#' 
#'
#' @inheritParams test_mcmc_reversible
#' @param paths Number of MCMC chains on which to estimate autocorrelation
#' @param steps Number of steps to take within each chain
#' @param ... Additional arguments to pass to \code{\link[stats]{acf}}.
#' @return A dataframe giving estimated autocorrelation per chain, per test 
#' statistic
#' @export
sample_acfs <- function(object, paths = 50, steps = 500, thinning = 1, ...) {
  check_object(object)
  object <- update_object(object)
    
  out <- lapply(seq_len(paths), sample_acf, object, steps, thinning, ...)
  out <- do.call("rbind", out)
  return(out)
}

# Estimate autocorrelation for one chain applied to one sample of parameters 
# and data
#
# Internal
#
# @inheritParams sample_acf
# @param n An 'id' to give to this chain
sample_acf <- function(n, object, steps = 500, thinning = 1, ...) {
  x <- object$genprior()
  y <- object$gendata(x)
  t0 <- object$test(x)
  res <- matrix(nrow = length(t0), ncol = steps)
  res[,1] <- t0
  for (i in 2:steps) {
    x <- object$stepMCMC(x, y, thinning)
    res[,i] <- object$test(x)
  }
  out <- apply(res, 1, function(x) stats::acf(x, plot = F, ...)$acf)
  out <- as.data.frame(out)
  colnames(out) <- seq_len(length(t0))
  out$lag <- seq_len(nrow(out)) - 1
  out$id <- n
  out <- tidyr::pivot_longer(out, cols = seq_len(length(t0)), 
                             names_to = "stat", values_to = "corr")
  out$stat <- as.integer(out$stat)
  out <- dplyr::arrange(out, .data$stat, .data$lag)
  return(out)
}


# Determine a lag such that the ACF is low enough
#
# Internal function.
#
# @inherit mc_test
# @param acfbound Cutoff sample autocorrelation between consecutive samples
# @return A nonnegative integer indicating number of steps required between
# consecutive samples.
getthinning <- function(object, control, acfbound = 0.5) {
  thinning <- 1
  theta <- object$genprior()
  dat <- object$gendata(theta)
  yakt <- object$test(theta)
  res <- matrix(
    nrow = length(yakt),
    ncol = 500
  )
  res[, 1] <- yakt
  for (i in 2:500) {
    theta <- object$stepMCMC(theta, dat, 1)
    res[, i] <- object$test(theta)
  }
  thinning <- sapply(seq_along(yakt), function(i) {
    cor <- stats::acf(res[i, ], plot = FALSE)
    if (all(cor$acf > acfbound)) {
      stop(paste(
        "Could not automatically compute thinning.",
        "Autocorrelation does not decrease below ",
        acfbound, "in", length(cor) - 1, "steps.\n"
      ), call. = FALSE)
    }

    min(which(c(cor$acf) <= acfbound)) - 1
  })

  if (any(is.na(thinning))) {
    stop("Could not automatically compute step size.\n", call. = FALSE)
    return(-1)
  }
  else {
    if (!is.null(control$verbose) && control$verbose > 0) {
      message("Step size thinning=", thinning, " chosen.")
    }
    return(max(thinning))
  }
}
