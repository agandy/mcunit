# Resubmission

Thank you for reviewing mcunit.

* This package (v0.3.1) was archived on CRAN on 2021-03-07 due to a failing test
* This has been addressed, with all tests passing.
* Version has been incremented to v0.3.2
* mis-spelled word 'Gandy' in DESCRIPTION is a proper name