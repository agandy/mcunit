# mcunit 0.3.4

* allow mcmc tests to give kernels without thinning as argument to simplify usage 


# mcunit 0.3.3

* Separated `expect_` functions from `test_` functions, to allow performing 
  tests outside of a unit testing environment.

* Parallel implementation of the tests.

* Function `plot_rank_hist` to visualise results of the rank test

* General refactoring of code

# mcunit 0.3.2

* Setting seed in test files in order to avoid false fails

# mcunit 0.3.1

* added `expect_bernoulli()` and vignette to test e.g. levels of tests
  and coverage probabilities of confidence intervals

# mcunit 0.3
 
* renamed `expect_mcmc_non_reversible()` to `expect_mcmc()`

* change of package name to `mcunit` from `MCUnit`
 
# MCunit 0.2

# Initial version.
