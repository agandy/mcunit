---
output: md_document
---


# mcunit

The goal of mcunit is to provide unit tests for MCMC and Monte Carlo
methods. It extends the package testthat.

## Installation

You can install the current  version of mcunit from [bitbucket](https://bitbucket.org) with:

``` r
devtools::install_bitbucket("agandy/mcunit")
```


## Example

This shows how to test if a sampler has a specific mean:


```r
sampler <- function(n) rnorm(n,mean=3.2,1)
expect_mc_iid_mean(sampler,mean=3.2)
expect_mc_iid_mean(sampler,mean=3.5)
#> Error: Test failed with p-value < 2.22e-16 in iteration 1
#> 
#> Iteration 1 (1000 samples)
#> Rejection if p <= 1.428571e-06; No Rejection if p > 0.1462144.
#> Estimated mean: 3.218531 s.e.: 0.03267932; Expected mean: 3.5.
```

This shows check of a simple MCMC sampler

```r
object <- list(genprior=function() rnorm(1),
               gendata=function(theta) rnorm(5,theta),
               stepMCMC=function(theta,data){
                   f <- function(x) prod(dnorm(data,x))*dnorm(x)  
		   thetanew = rnorm(1,mean=theta,sd=1)
                   if (runif(1)<f(thetanew)/f(theta))
                        theta <- thetanew
                   theta
               }
               )
expect_mcmc_reversible(object)
## And now with an error in the sampler:
## sampling until sample is accepted.
object$stepMCMC <- function(theta,data,thinning){
    f <- function(x) prod(dnorm(data,x))*dnorm(x)  
    repeat{
          thetanew = rnorm(1,mean=theta,sd=1)            
          if (runif(1)<f(thetanew)/f(theta)) break;
    }
    thetanew
}
expect_mcmc_reversible(object,control=list(n=1e4))
#> Error: Test failed with p-value < 2.22e-16 in iteration 2
#> 
#> Iteration 2 (40000 samples)
#> Rejection if p <= 9.770481e-06; No Rejection if p > 0.1462228.
```

To the samples returned from the sampler and get some diagnostic:

```r
res <- expect_mcmc_reversible(object,
         control=list(n=1e4,returnsamples=TRUE))
res
#> FAIL - Rejecting hypothesis that sampler produces the expected behaviour.
#> Last iteration:
#> Iteration 1 (10000 samples)
#> Rejection if p <= 1.428571e-06; No Rejection if p > 0.1462144.
#> p= 3.282705e-08
```

<!-- README.md is generated from README.Rmd. Please edit that file -->
